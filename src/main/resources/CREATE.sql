
CREATE TABLE student
(
    id INT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(45),
    birthday DATE NOT NULL,
    groupnumber INT(11) NOT NULL
);

CREATE TABLE subject
(
    id INT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(250),
    description VARCHAR(255),
    grade int(11) NOT NULL
);

CREATE TABLE mark
(
    id INT(11) NOT NULL AUTO_INCREMENT ,
    student_id INT,
    subject_id INT,
    mark int(11) NOT NULL,
    foreign key (student_id) references student(id),
    foreign key (subject_id) references subject(id)
);

CREATE TABLE paymenttype
(
    id INT(11) NOT NULL,
    name VARCHAR(45),
);

CREATE TABLE payment
(
    id INT(11) NOT NULL AUTO_INCREMENT,
    type INT(11) NOT NULL,
    amount numeric NOT NULL,
    payment_date TIMESTAMP  NOT NULL,
    student_id INT NOT NULL,
    foreign key (type) references paymenttype(id),
    foreign key (student_id) references student(id),
);
