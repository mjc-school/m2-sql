package mjc.mapper;

import mjc.entity.Subject;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SubjectMapper implements RowMapper<Subject> {
    @Override
    public Subject mapRow(ResultSet resultSet, int i) throws SQLException {
        Subject subject = new Subject();
        subject.setId(resultSet.getLong("id"));
        subject.setName(resultSet.getString("name"));
        subject.setDescription(resultSet.getString("description"));
        subject.setGrade(resultSet.getString("grade"));

        return subject;
    }
}
