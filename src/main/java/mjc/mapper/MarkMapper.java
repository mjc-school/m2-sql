package mjc.mapper;

import mjc.entity.Mark;
import mjc.entity.Student;
import mjc.entity.Subject;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MarkMapper implements RowMapper<Mark> {
    @Override
    public Mark mapRow(ResultSet resultSet, int i) throws SQLException {
        Mark mark = new Mark();
        try {
            mark.setId(resultSet.getLong("id"));

        } catch (Exception ignored) {

        }
        try {
            Long studentId = resultSet.getLong("student_id");
            Student student = new Student(studentId);
            mark.setStudent(student);
        } catch (Exception ignored) {

        }
        try {
            mark.setMark(resultSet.getInt("mark"));

        } catch (Exception ignored) {

        }
        try {
            Long subjectId = resultSet.getLong("subject_id");
            Subject subject = new Subject();
            subject.setId(subjectId);
            mark.setSubject(subject);
        } catch (Exception ignored) {

        }

        return mark;
    }
}
