package mjc.mapper;

import mjc.entity.PaymentType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PaymentTypeMapper implements RowMapper<PaymentType> {
    @Override
    public PaymentType mapRow(ResultSet resultSet, int i) throws SQLException {
        PaymentType paymentType = new PaymentType();
        try {
            paymentType.setId(resultSet.getLong("id"));
        } catch (Exception ignored) { }
        try {
            paymentType.setName(resultSet.getString("name"));
        } catch (Exception ignored) { }
        return paymentType;
    }
}
