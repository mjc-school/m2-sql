package mjc.mapper;

import mjc.entity.Student;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;

public class StudentMapper implements RowMapper<Student> {
    @Override
    public Student mapRow(ResultSet resultSet, int i) {
        Student student = new Student();
        Long id = null;
        String name = null;
        Date birthday = null;
        Integer group = null;
        try {
            id = resultSet.getLong("id");
        } catch (Exception ignored) {
        }
        student.setId(id);
        try {
            name = resultSet.getString("NAME");
        } catch (Exception ignored) {
        }
        student.setName(name);
        try {
            birthday = resultSet.getDate("birthday");
        } catch (Exception ignored) {
        }
        student.setBirthday(birthday);
        try {
            group = resultSet.getInt("groupnumber");
        } catch (Exception ignored) {
        }
        student.setGroup(group);

        return student;
    }
}
