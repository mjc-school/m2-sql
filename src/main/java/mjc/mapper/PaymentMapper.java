package mjc.mapper;

import mjc.entity.Payment;
import mjc.entity.PaymentType;
import mjc.entity.Student;
import org.springframework.jdbc.core.RowMapper;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PaymentMapper implements RowMapper<Payment> {
    @Override
    public Payment mapRow(ResultSet resultSet, int i) {
        Long id = null;
        Long type = null;
        BigDecimal amount = null;
        Date paymentDate = null;
        Long studentId = null;
        try {
            id = resultSet.getLong("id");
        } catch (Exception ignored) {
        }
        Payment payment = new Payment();
        payment.setId(id);

        try {
            type = resultSet.getLong("type");
        } catch (Exception ignored) {
        }
        PaymentType paymentType = new PaymentType();
        paymentType.setId(type);
        payment.setType(paymentType);
        try {
            amount = resultSet.getBigDecimal("amount");
        } catch (Exception ignored) {
        }
        payment.setAmount(amount);
        try {
            paymentDate = resultSet.getDate("payment_date");
        } catch (Exception ignored) {
        }
        payment.setDate(paymentDate);
        try {
            studentId = resultSet.getLong("student_id");
        } catch (Exception ignored) {
        }
        Student student = new Student();
        student.setId(studentId);
        payment.setStudent(student);

        return payment;
    }
}
