package mjc.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Payment extends Entity {
    private PaymentType type;
    private BigDecimal amount;
    private Student student;
    private Date date;

    public Payment() {
    }

    public Payment(PaymentType type, BigDecimal amount, Student student, Date date) {
        this.type = type;
        this.amount = amount;
        this.student = student;
        this.date = date;
    }

    public PaymentType getType() {
        return type;
    }

    public void setType(PaymentType type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
