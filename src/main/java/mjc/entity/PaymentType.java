package mjc.entity;

public class PaymentType extends Entity {
    private String name;

    public PaymentType() {
    }

    public PaymentType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
