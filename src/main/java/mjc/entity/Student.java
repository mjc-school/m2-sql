package mjc.entity;

import java.util.Date;

public class Student extends Entity {
    private String name;
    private Date birthday;
    private Integer group;

    public Student() {
    }

    public Student(String name, Date birthday, Integer group) {
        this.name = name;
        this.birthday = birthday;
        this.group = group;
    }

    public Student(Long studentId) {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }
}
