package mjc.entity;

public class Mark extends Entity {
    private Student student;
    private Subject subject;
    private Integer mark;

    public Mark() {
    }

    public Mark(Student student, Subject subject, Integer mark) {
        this.student = student;
        this.subject = subject;
        this.mark = mark;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }
}
