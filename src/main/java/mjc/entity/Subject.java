package mjc.entity;

public class Subject extends Entity {
    private String name;
    private String description;
    private String grade;

    public Subject() {
    }

    public Subject(String name, String description, String group) {
        this.name = name;
        this.description = description;
        this.grade = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", grade='" + grade + '\'' +
                ", id=" + id +
                '}';
    }
}
