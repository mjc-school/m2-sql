package mjc;

import mjc.dao.SqlConnection;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AggregateFunctionsTest {
    @Test
    public void selectMaxDateOfBirthTest() {
        //given
        String givenQuery = "Select MAX(birthday) FROM Student ";

        //when
        LocalDate localDate = SqlConnection.getConnection().queryForObject(givenQuery, LocalDate.class);

        //then
        assertEquals(LocalDate.of(2002, 8, 29), localDate);
    }

    @Test
    public void selectMostEarlyOrderDateTest() {
        //given
        String givenQuery = "Select MIN(payment_date) FROM Payment ";

        //when
        LocalDate localDate = SqlConnection.getConnection().queryForObject(givenQuery, LocalDate.class);

        //then
        assertEquals(LocalDate.of(2015, 9, 15), localDate);
    }

    @Test
    public void selectAverageMarkForMathTest() {
        //given
        String givenQuery = "Select AVG(mark) FROM Mark Where subject_id = 104 ";

        //when
        Double avgMark = SqlConnection.getConnection().queryForObject(givenQuery, Double.class);

        //then
        assertEquals(6.0, avgMark);
    }

    @Test
    public void selectMinAmountOfPaymentForWeeklyTypeTest() {
        //given
        String givenQuery = "Select MIN(amount) FROM Payment Join  PaymentType on PaymentType.id = Payment.type Where PaymentType.name = 'WEEKLY'  ";

        //when
        Double minAmount = SqlConnection.getConnection().queryForObject(givenQuery, Double.class);

        //then
        assertEquals(19.78, minAmount);
    }


}
