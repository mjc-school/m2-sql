package mjc.connection;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;
import java.util.Objects;

public class SqlConnectionWithEmptyDB {
    private static DataSource dataSource;

    private SqlConnectionWithEmptyDB(){
        dataSource = getDataSource();
    }

    public static JdbcTemplate getConnection() {
        if (Objects.isNull(dataSource)) {
            dataSource = getDataSource();
        }

        return new JdbcTemplate(dataSource);
    }

    private static DataSource getDataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .build();
    }
}
