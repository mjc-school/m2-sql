package mjc;

import mjc.dao.SqlConnection;
import mjc.entity.Mark;
import mjc.entity.Payment;
import mjc.entity.PaymentType;
import mjc.entity.Student;
import mjc.mapper.MarkMapper;
import mjc.mapper.PaymentMapper;
import mjc.mapper.PaymentTypeMapper;
import mjc.mapper.StudentMapper;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderByTest {
    @Test
    public void selectMarksMoreThan6OrderByDESCTest() {
        //given
        String givenQuery = "Select mark from MARK where mark > 6 ORDER BY mark DESC;";

        //when
        List<Mark> actualResult = SqlConnection.getConnection().query(givenQuery, new MarkMapper());

        //then
        assertTrue(actualResult.size() > 0);
        assertEquals(0, actualResult.stream().filter(m -> m.getMark() < 6).count());
        for (int i = 0; i < actualResult.size() - 1; i++) {
            assertTrue(actualResult.get(i).getMark() >= actualResult.get(i + 1).getMark());
        }
    }

    @Test
    public void selectPaymentsLess300FilterInASCTest() {
        //given
        String givenQuery = "Select amount from Payment where amount < 300 ORDER BY amount ASC;";

        //when
        List<Payment> actualResult = SqlConnection.getConnection().query(givenQuery, new PaymentMapper());

        //then
        assertTrue(actualResult.size() > 0);
        assertEquals(0, actualResult.stream().filter(p -> p.getAmount().intValue() > 300).count());

        for (int i = 0; i < actualResult.size() - 1; i++) {
            assertTrue(actualResult.get(i).getAmount().intValue() <= actualResult.get(i + 1).getAmount().intValue());
        }
    }

    @Test
    public void selectPaymentTypesSortedByAlphabetTest() {
        //given
        String givenQuery = "Select name from PaymentType  ORDER BY name ASC";

        //when
        List<PaymentType> actualResult = SqlConnection.getConnection().query(givenQuery, new PaymentTypeMapper());

        //then
        assertTrue(actualResult.size() > 0);
        assertEquals("MONTHLY", actualResult.get(0).getName());
        assertEquals("YEAR", actualResult.get(3).getName());
    }

    @Test
    public void selectStudentsInReverseAlphabeticalOrderTest() {
        //given
        String givenQuery = "Select id, name, birthday, groupNumber from Student ORDER BY name DESC";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());

        //then
        assertTrue(actualResult.size() > 0);
        assertEquals("Zsazsa Britcher", actualResult.get(0).getName());
    }

    @Test
    public void selectStudentsWhoHaveAtLeast1PaymentMoreThan1000OrderByBirthday() {
        //given
        String givenQuery = "SELECT student.id" +
                "                from Student INNER JOIN Payment on student.id = payment.student_id " +
                "WHERE payment.amount > 1000 " +
                "GROUP BY student.id, student.birthday  ORDER BY student.birthday ASC";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());

        //then
        assertEquals(1317, (long) actualResult.get(0).getId());
        assertEquals(1014, (long) actualResult.get(1).getId());
    }
}
