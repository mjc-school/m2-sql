package mjc;

import mjc.dao.SqlConnection;
import mjc.entity.Mark;
import mjc.entity.Student;
import mjc.mapper.MarkMapper;
import mjc.mapper.StudentMapper;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DeleteTest {

    @Test
    public void alterTablesForDelete() {

    }

    @Test
    public void deleteAllStudentsWithGradeMoreThan4AndAllRelationsTest() {
        //given
        String preconditions = "ALTER TABLE payment " +
                "DROP CONSTRAINT payment_student_id_fkey, " +
                "ADD CONSTRAINT payment_student_id_fkey " +
                "FOREIGN KEY (student_id) " +
                "REFERENCES student(id) " +
                "ON DELETE CASCADE;";
        String preconditions2 = "ALTER TABLE mark " +
                "DROP CONSTRAINT mark_student_id_fkey, " +
                "ADD CONSTRAINT mark_student_id_fkey " +
                "   FOREIGN KEY (student_id) " +
                "   REFERENCES student(id) " +
                "   ON DELETE CASCADE;";
        String givenQuery ="DELETE FROM Student " +
                "Using mark " +
                "Where mark.mark < 4 AND mark.student_id = student.id";
        String previousNumberOfStudents = "Select *  FROM STudent " +
                "LEFT Join mark on mark.student_id = student.id " +
                "Where mark.mark < 4 ";

        //when
        List<Student> previousNumberOfStudentsResult = SqlConnection.getConnection().query(previousNumberOfStudents, new StudentMapper());
        //SqlConnection.getConnection().execute(preconditions);
        //SqlConnection.getConnection().execute(preconditions2);

        SqlConnection.getConnection().execute(givenQuery);
        List<Student> actualNumberOfStudentsResult = SqlConnection.getConnection().query(previousNumberOfStudents, new StudentMapper());

        //then
        assertTrue(actualNumberOfStudentsResult.size() == 0, ErrorMessageConst.DELETE_QUERY_DID_NOT_WORKED);



    }

    @Test
    public void deleteStudentsWhoHaveAtLeastOneMarkLessThan4Test() {

    }

    @Test
    public void DeleteDailyPaymentTypeAndAllRelationsTest() {

    }

    @Test
    public void DeleteAllMarksLessThan4() {
        //given
        String givenQuery = "DELETE FROM MARK WHERE mark < 4";
        String resultAfterQueryExecutionQuery = "SELECT * from MARK where mark < 4";

        //when
        SqlConnection.getConnection().execute(givenQuery);
        List<Mark> resultAfterQueryExecution = SqlConnection.getConnection().query(resultAfterQueryExecutionQuery, new MarkMapper());

        //then
        assertTrue(resultAfterQueryExecution.size() == 0, ErrorMessageConst.DELETE_QUERY_DID_NOT_WORKED);

    }
}
