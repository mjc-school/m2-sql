package mjc;

import mjc.dao.SqlConnection;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.jdbc.core.JdbcTemplate;

public class InsertQueryTest {
    private final static String CHECK_QUERY = "select count(*) from ";
    private final static String STUDENT = "student";
    private final static String SUBJECT = "subject";
    private final static String MARK = "mark";
    private final static String PAYMENT_TYPE = "paymenttype";
    private final static String PAYMENT = "payment";

    @Test
    public void studentInsertTest() {
        validateInsert(STUDENT);
    }

    @Test
    public void subjectInsertTest() {
        validateInsert(SUBJECT);
    }

    @Test
    public void markInsertTest() {
        validateInsert(MARK);
    }

    @Test
    public void paymentTypeInsertTest() {
        validateInsert(PAYMENT_TYPE);
    }

    @Test
    public void paymentInsertTest() {
        validateInsert(PAYMENT);
    }

    private void validateInsert(String tableName) {
        //given
        String autoCodeQuery = "";

        //when
        JdbcTemplate connection = SqlConnection.getConnection();

        Integer beforeInserting = connection.queryForObject(CHECK_QUERY + tableName, Integer.class);
        connection.execute(autoCodeQuery);
        Integer afterInserting = connection.queryForObject(CHECK_QUERY + tableName, Integer.class);

        //then
        Assertions.assertTrue(afterInserting > beforeInserting);
    }
}
