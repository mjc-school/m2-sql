package mjc;

import mjc.dao.SqlConnection;
import mjc.entity.Mark;
import mjc.entity.Payment;
import mjc.entity.Student;
import mjc.entity.Subject;
import mjc.mapper.MarkMapper;
import mjc.mapper.PaymentMapper;
import mjc.mapper.StudentMapper;
import mjc.mapper.SubjectMapper;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static mjc.ErrorMessageConst.INCORRECT_SYNTAX;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UpdateTest {
    @Test
    void updateSubjectSetGrade5() {
        //given
        String givenQuery = "Update Subject set grade = 5 where name = 'End of Suburbia: Oil Depletion and the Collapse of the American Dream'";

        //when
        SqlConnection.getConnection().execute(givenQuery);

        List<Subject> subject = SqlConnection.getConnection().query("Select * from Subject where name = 'End of Suburbia: Oil Depletion and the Collapse of the American Dream'", new SubjectMapper());

        //then
        assertNotNull(subject);
        assertEquals(subject.size(), 1);
        assertTrue(givenQuery.contains("set") || givenQuery.contains("SET"), INCORRECT_SYNTAX);
        assertNotNull(subject.get(0).getName(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
        assertEquals("5", subject.get(0).getGrade());
    }

    @Test
    void updateStudentSetBirthday() {
        //given
        String givenQuery = "Update Student set groupNumber = 8 where name = 'Ernestus Mozzi'";

        //when
        SqlConnection.getConnection().execute(givenQuery);

        List<Student> students = SqlConnection.getConnection().query("Select * from Student where name = 'Ernestus Mozzi'", new StudentMapper());

        //then
        assertNotNull(students);
        assertEquals(students.size(), 1);
        assertTrue(givenQuery.contains("set") || givenQuery.contains("SET"), INCORRECT_SYNTAX);

        assertEquals(8, students.get(0).getGroup());
    }

    @Test
    void updatePaymentSetAmountAndStudentIdWhereDate() {
        //given
        String givenQuery = "Update Payment set amount = 500, student_id = 2 where payment_date > '2021-01-01'";

        //when
        SqlConnection.getConnection().execute(givenQuery);

        List<Payment> payments = SqlConnection.getConnection().query("Select * from Payment where payment_date > '2021-01-01' and type = 2", new PaymentMapper());

        //then
        assertEquals(payments.size(), 86);
        assertTrue(givenQuery.contains("set") || givenQuery.contains("SET"), INCORRECT_SYNTAX);
        assertTrue(payments.get(5).getAmount().equals(new BigDecimal("500")));
        assertEquals(payments.get(55).getStudent().getId(), 2);
    }

    @Test
    void updateMarkSetMark2JoinOnTableSubject() {
        //given
        String givenQuery = "Update Mark SET mark = 2 where subject_id = 315";

        //when
        SqlConnection.getConnection().execute(givenQuery);
        List<Mark> marks = SqlConnection.getConnection().query("Select * from Mark where subject_id = 315", new MarkMapper());

        //then
        assertEquals(marks.size(), 11);
        assertTrue(givenQuery.contains("set") || givenQuery.contains("SET"), INCORRECT_SYNTAX);
        assertEquals(marks.get(5).getMark(), 2);
        assertEquals(marks.get(10).getMark(), 2);
    }
}