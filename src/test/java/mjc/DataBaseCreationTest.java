package mjc;

import mjc.connection.SqlConnectionWithEmptyDB;
import mjc.mapper.TableMapper;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Arrays;
import java.util.List;

public class DataBaseCreationTest {
    private final static String CHECK_QUERY =
            "select TABLE_NAME from INFORMATION_SCHEMA.TABLES " +
                    "where TABLE_NAME in ('STUDENT', 'SUBJECT', 'MARK', 'PAYMENTTYPE', 'PAYMENT')";

    @Test
    public void dataBaseCreationTest() {
        //given
        String autoCodeQuery = "";
        List<String> checkTables = Arrays.asList("STUDENT", "SUBJECT", "MARK", "PAYMENTTYPE", "PAYMENT");

        //when
        JdbcTemplate connection = SqlConnectionWithEmptyDB.getConnection();

        connection.execute(autoCodeQuery);
        List<String> tables = connection.query(CHECK_QUERY, new TableMapper());

        tables.sort(String::compareTo);
        checkTables.sort(String::compareTo);

        //then
        Assertions.assertEquals(checkTables, tables);
    }
}
