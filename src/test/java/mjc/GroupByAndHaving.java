package mjc;

import mjc.dao.SqlConnection;
import mjc.entity.Student;
import mjc.mapper.StudentMapper;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GroupByAndHaving {

    @Test
    void selectAllGroupsFromStudentsHavingAverageMarkMoreThan5() {
        //given
        String givenQuery = " Select s.id, s.name from Student as s " +
                "Left Join Mark on s.id = mark.student_id " +
                "group by s.name, s.id " +
                "having AVG(mark.mark) > 8";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertEquals(130, actualResult.size());
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("Clarice Gaishson")));
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("Esra Gowrich")));
    }

    @Test
    void selectBestStudentsHavingMinimumMarkMoreThan7() {
        //given
        String givenQuery = "select Student.id, Student.name, MIN(mark) " +
                "from Student " +
                "Join Mark on Student.id = Mark.student_id " +
                "Group by Student.id, student.name " +
                "Having MIN(mark) > 7";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertEquals(188, actualResult.size());
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("Mike Hatter")));
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("Baryram Januszewski")));
    }

    @Test
    void selectStudentsWhoPaidMoreThan2TimesIn2019() {
        //given
        String givenQuery = "select Student.id, student.name " +
                "From Student " +
                "Join Payment on Payment.student_id = Student.id " +
                "Where Payment.payment_date > '2019-01-01' AND Payment.payment_date < '2019-12-31' " +
                "Group by Student.id, Student.name " +
                "Having COUNT(Student.id) > 2";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertEquals(7, actualResult.size());
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("Barrie Willmont")));
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("Fransisco De Winton")));
    }


}