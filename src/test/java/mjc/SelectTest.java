package mjc;

import mjc.dao.SqlConnection;
import mjc.entity.Payment;
import mjc.entity.Student;
import mjc.mapper.PaymentMapper;
import mjc.mapper.StudentMapper;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

public class SelectTest {

    @Test
    public void selectAllStudentsTest() {
        //given
        String givenQuery = "Select * from student";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());
        List<Student> expectedResult = SqlConnection.getConnection().query("Select * from Student", new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(2000, expectedResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertTrue(givenQuery.contains("*"), ErrorMessageConst.INCORRECT_QUERY_USING_COLUMNS_ENUM);
        assertNotNull(actualResult.get(1).getName(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
    }

    @Test
    public void select50StudentsTest() {
        //given
        String givenQuery = "Select * from student LIMIT 50";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(50, actualResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertTrue(givenQuery.contains("*"), ErrorMessageConst.INCORRECT_QUERY_USING_COLUMNS_ENUM);
        assertNotNull(actualResult.get(1).getName(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
    }

    @Test
    public void selectOnlyStudentsNames() {
        //given
        String givenQuery = "Select name from student";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(2000, actualResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertNotNull(actualResult.get(1).getName(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
    }

    @Test
    public void selectUniqueAmountFromOrder() {
        String givenQuery = "Select DISTINCT Amount From Payment";

        List<Payment> actualResult = SqlConnection.getConnection().query(givenQuery, new PaymentMapper());

        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(2995, actualResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertNotNull(actualResult.get(1).getAmount(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
    }
}
