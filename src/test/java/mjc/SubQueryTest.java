package mjc;

import mjc.dao.SqlConnection;
import mjc.entity.Student;
import mjc.entity.Subject;
import mjc.mapper.StudentMapper;
import mjc.mapper.SubjectMapper;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SubQueryTest {
    @Test
    void selectSubjectsWhereAverageMarkIsMoreThanAverageMarkForAll() {
        //given
        String givenQuery = "select Subject.id, Subject.name, Subject.description, Subject.grade " +
                "From Subject " +
                "Join Mark on Subject.id = Mark.subject_id " +
                "GROUP by Subject.id, Subject.name, Subject.description, Subject.grade " +
                "HAVING AVG(mark) > (Select AVG(mark) from mark) ";

        //when
        List<Subject> actualResult = SqlConnection.getConnection().query(givenQuery, new SubjectMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("House of Games")));
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("Lunopolis")));
    }

    @Test
    void selectStudentsPaidMoreThanAverageAmountForAll() {
        //given
        String givenQuery = "select Student.id, student.name " +
                "From Student " +
                "Join Payment on Payment.student_id = Student.id " +
                "GROUP by Student.id, Student.name " +
                "HAVING AVG(amount) < (Select AVG(amount) from Payment) ";
        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertEquals(765, actualResult.size());
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("Vince Beachamp")));
        assertTrue(actualResult.stream().anyMatch(a -> a.getName().equals("Emanuele Lennie")));
    }
}
