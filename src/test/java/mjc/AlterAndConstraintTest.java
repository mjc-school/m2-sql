package mjc;

import mjc.dao.SqlConnection;
import org.junit.jupiter.api.Test;
import org.springframework.dao.DataIntegrityViolationException;

import static mjc.ErrorMessageConst.ALTER_QUERY_INCORRECT;
import static mjc.ErrorMessageConst.INCORRECT_SYNTAX;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AlterAndConstraintTest {

    //        -	Change table ‘Student’.
//        ‘Birthdate’ field cannot be null
    @Test
    void changeStudentBirthDayNotNull() {
        //given
        String givenQueryAlter = "ALTER TABLE Student MODIFY birthday DATE NOT NULL";
        String queryUpdate = "UPDATE Student SET birthday = NULL WHERE name = 'Debor Lamey' ";
        String preConditionAlter = "ALTER TABLE Student MODIFY birthday DATE NULL";
        SqlConnection.getConnection().execute(preConditionAlter);

        //when
        SqlConnection.getConnection().execute(givenQueryAlter);

        //then
        assertNotNull(givenQueryAlter);
        assertTrue(givenQueryAlter.contains("ALTER") || givenQueryAlter.contains("alter"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertTrue(givenQueryAlter.contains("MODIFY") || givenQueryAlter.contains("modify"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertTrue(givenQueryAlter.contains("NOT NULL") || givenQueryAlter.contains("not null"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        try {
            SqlConnection.getConnection().execute(queryUpdate);
            assertNull(ALTER_QUERY_INCORRECT, ALTER_QUERY_INCORRECT);
        } catch (DataIntegrityViolationException exception) {
        }
    }

    @Test
    void changeMarkRange() {
        //given
        String givenQueryAlter = "ALTER TABLE Mark ADD CHECK (mark>=1 and mark <= 10); " +
                "ALTER TABLE Mark MODIFY student_id INT NOT NULL; " +
                "ALTER TABLE Mark MODIFY subject_id INT NOT NULL";
        String queryUpdate = "UPDATE Mark SET mark = 11, student_id = NULL, subject_id = NULL WHERE student_id = 1688 and subject_id = 374";
        String preConditionAlter = "ALTER TABLE Mark MODIFY student_id INT NULL; " +
                "ALTER TABLE Mark MODIFY subject_id INT NULL";
        SqlConnection.getConnection().execute(preConditionAlter);

        //when
        SqlConnection.getConnection().execute(givenQueryAlter);

        //then
        assertNotNull(givenQueryAlter);
        assertTrue(givenQueryAlter.contains("ALTER") || givenQueryAlter.contains("alter"), INCORRECT_SYNTAX + "ALTER");
        assertTrue(givenQueryAlter.contains("ADD") || givenQueryAlter.contains("add"), INCORRECT_SYNTAX + "ADD");
        assertTrue(givenQueryAlter.contains("CHECK") || givenQueryAlter.contains("check"), INCORRECT_SYNTAX + "CHECK");
        assertTrue(givenQueryAlter.contains("MODIFY") || givenQueryAlter.contains("modify"), INCORRECT_SYNTAX + "MODIFY");

        try {
            SqlConnection.getConnection().execute(queryUpdate);
            assertNull(ALTER_QUERY_INCORRECT, ALTER_QUERY_INCORRECT);
        } catch (DataIntegrityViolationException exception) {
        }
    }

    @Test
    void changeSubjectCourseRange() {
        //given
        String givenQueryAlter = "ALTER TABLE Subject ADD CHECK (grade>=1 and grade <= 10); ";
        String queryUpdate = "UPDATE Subject SET grade = 11 WHERE name = 'Son of the White Mare' ";

        //when
        SqlConnection.getConnection().execute(givenQueryAlter);

        //then
        assertNotNull(givenQueryAlter);
        assertTrue(givenQueryAlter.contains("ALTER") || givenQueryAlter.contains("alter"), INCORRECT_SYNTAX + "ALTER");
        assertTrue(givenQueryAlter.contains("ADD") || givenQueryAlter.contains("add"), INCORRECT_SYNTAX + "ADD");
        assertTrue(givenQueryAlter.contains("CHECK") || givenQueryAlter.contains("check"), INCORRECT_SYNTAX + "CHECK");
        try {
            SqlConnection.getConnection().execute(queryUpdate);
            assertNull(ALTER_QUERY_INCORRECT, ALTER_QUERY_INCORRECT);
        } catch (DataIntegrityViolationException exception) {
        }
    }

    @Test
    void changeTablePaymentTypeNameUnique() {
        //given
        String givenQueryAlter = "ALTER TABLE paymenttype ADD UNIQUE (name)";
        String queryUpdate = "UPDATE paymenttype SET name = 'REWARD'";

        //when
        SqlConnection.getConnection().execute(givenQueryAlter);

        //then
        assertNotNull(givenQueryAlter);
        assertTrue(givenQueryAlter.contains("ALTER") || givenQueryAlter.contains("alter"), INCORRECT_SYNTAX + "ALTER");
        assertTrue(givenQueryAlter.contains("ADD") || givenQueryAlter.contains("add"), INCORRECT_SYNTAX + "ADD");
        assertTrue(givenQueryAlter.contains("UNIQUE") || givenQueryAlter.contains("unique"), INCORRECT_SYNTAX + "UNIQUE");

        try {
            SqlConnection.getConnection().execute(queryUpdate);
            assertNull(ALTER_QUERY_INCORRECT, ALTER_QUERY_INCORRECT);
        } catch (DataIntegrityViolationException exception) {
        }
    }

    @Test
    void changePaymentTypeDateAndAmountNotNull() {
        //given
        String givenQueryAlter = "ALTER TABLE Payment MODIFY Type INT NOT NULL; " +
                "ALTER TABLE Payment MODIFY Amount numeric NOT NULL; " +
                "ALTER TABLE Payment MODIFY payment_date TIMESTAMP NOT NULL";
        String queryUpdate = "UPDATE Payment SET Type = NULL, Amount = NULL, payment_date = NULL WHERE type = 3 and amount = 4969.33";
        String preConditionAlter = "ALTER TABLE Payment MODIFY Type INT NULL; " +
                "ALTER TABLE Payment MODIFY Amount numeric NULL; " +
                "ALTER TABLE Payment MODIFY payment_date TIMESTAMP NULL";
        SqlConnection.getConnection().execute(preConditionAlter);

        //when
        SqlConnection.getConnection().execute(givenQueryAlter);

        //then
        assertNotNull(givenQueryAlter);
        assertTrue(givenQueryAlter.contains("ALTER") || givenQueryAlter.contains("alter"), INCORRECT_SYNTAX + "ALTER");
        assertTrue(givenQueryAlter.contains("MODIFY") || givenQueryAlter.contains("modify"), INCORRECT_SYNTAX + "MODIFY");

        try {
            SqlConnection.getConnection().execute(queryUpdate);
            assertNull(ALTER_QUERY_INCORRECT, ALTER_QUERY_INCORRECT);
        } catch (DataIntegrityViolationException exception) {
        }
    }
}
