package mjc;

public class ErrorMessageConst {
    //common
    public static final String ZERO_SIZE = "Your query returned 0 result";
    public static final String INCORRECT_SIZE = "Your query returned incorrect size";
    public static final String INCORRECT_RETURNED_DATA = "Result data is incorrect";
    public static final String INCORRECT_SYNTAX = "Provided query does not have a key work ";

    //Select
    public static final String INCORRECT_QUERY_USING_COLUMNS_ENUM = "Use * instead of enumeration of columns";

    //where
    public static final String INCORRECT_CONDITION_IN_WHERE = "Provided query has incorrect condition";

    //alter constraints
    public static final String ALTER_QUERY_INCORRECT = "ALTER query didn't worked";

    //delete
    public static final String DELETE_QUERY_DID_NOT_WORKED = "Query you used did not worked. There are still some users returned according to the conditions provided in the task.";


}
