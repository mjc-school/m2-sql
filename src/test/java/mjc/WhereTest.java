package mjc;

import mjc.dao.SqlConnection;
import mjc.entity.Payment;
import mjc.entity.Student;
import mjc.mapper.PaymentMapper;
import mjc.mapper.StudentMapper;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WhereTest {
    @Test
    void selectOrdersWhereAmountMoreOrEqual500Test() {
        //given
        String givenQuery = "Select * from Payment where amount >= 500";

        //when
        List<Payment> actualResult = SqlConnection.getConnection().query(givenQuery, new PaymentMapper());
        List<Payment> expectedResult = SqlConnection.getConnection().query("Select * from Payment where amount >= 500", new PaymentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(2826, expectedResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertTrue(givenQuery.contains(">="), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertNotNull(actualResult.get(1).getId(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
    }

    @Test
    void whereStudentsOlderThan20Test() {
        //given
        String givenQuery = "Select * from student where birthday > dateadd(year, -20, getdate()) ";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());
        List<Student> expectedResult = SqlConnection.getConnection().query("Select * from student where birthday > dateadd(year, -20, getdate())", new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(31, expectedResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertTrue(givenQuery.contains(">"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertNotNull(actualResult.get(1).getName(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
    }

    @Test
    void whereStudentsOlderThan20AndGroup10Test() {
        //given
        String givenQuery = "Select * from student where birthday > dateadd(year, -20, getdate()) AND groupnumber = 10 ";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());
        List<Student> expectedResult = SqlConnection.getConnection().query("Select * from student where birthday > dateadd(year, -20, getdate()) AND groupnumber = 10", new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(4, expectedResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertTrue(givenQuery.contains("AND"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertNotNull(actualResult.get(1).getName(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
    }

    @Test
    void whereNameMikeORFromGroupTest() {
        //given
        String givenQuery = "Select * from student where name LIKE '%Mike%' OR groupNumber = 4 OR groupNumber = 5 OR groupNumber = 6";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());
        List<Student> expectedResult = SqlConnection.getConnection()
                .query("Select * from student where name LIKE '%Mike%' OR groupNumber = 4 OR groupNumber = 5 OR groupNumber = 6", new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(590, expectedResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertTrue(givenQuery.contains("OR") || givenQuery.contains("or"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertNotNull(actualResult.get(1).getName(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
    }

    @Test
    void wherePaymentsConductedInPast2Years() {
        //given
        String givenQuery = "Select * from Payment where payment_date > dateadd(year, -2, getdate())";

        //when
        List<Payment> actualResult = SqlConnection.getConnection().query(givenQuery, new PaymentMapper());
        List<Payment> expectedResult = SqlConnection.getConnection().query("Select * from Payment where payment_date > dateadd(year, -2, getdate())", new PaymentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(851, expectedResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertTrue(givenQuery.contains(">"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertNotNull(actualResult.get(1).getId(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
    }

    @Test
    void whereStudentNameStartsWithA() {
        //given
        String givenQuery = "Select * from student where name LIKE 'A%' ";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());
        List<Student> expectedResult = SqlConnection.getConnection()
                .query("Select * from student where name LIKE 'A%' ", new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(161, expectedResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertTrue(givenQuery.contains("'A%'"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertNotNull(actualResult.get(1).getName(), ErrorMessageConst.INCORRECT_RETURNED_DATA);

    }

    @Test
    void whereStudentNameIsRoxiAndGroup4OrNameTallieAndGroup9() {
        //given
        String givenQuery = "Select * from student where (name LIKE '%Roxi%' and groupNumber = 4) OR (name LIKE '%Tallie%' and groupNumber = 9)";

        //when
        List<Student> actualResult = SqlConnection.getConnection().query(givenQuery, new StudentMapper());
        List<Student> expectedResult = SqlConnection.getConnection()
                .query("Select * from student where (name LIKE '%Roxi%' and groupNumber = 4) OR (name LIKE '%Tallie%' and groupNumber = 9)", new StudentMapper());

        //then
        assertNotNull(actualResult);
        assertTrue(actualResult.size() > 0, ErrorMessageConst.ZERO_SIZE);
        assertEquals(1, expectedResult.size(), ErrorMessageConst.INCORRECT_SIZE);
        assertTrue(givenQuery.contains("'%Tallie%'"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertTrue(givenQuery.contains("AND") || givenQuery.contains("and"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertTrue(givenQuery.contains("OR") || givenQuery.contains("or"), ErrorMessageConst.INCORRECT_CONDITION_IN_WHERE);
        assertNotNull(actualResult.get(0).getName(), ErrorMessageConst.INCORRECT_RETURNED_DATA);
        assertEquals(actualResult.get(0).getName(), "Roxi Ivakhnov");
    }
}
