package mjc;

import mjc.dao.SqlConnection;
import mjc.entity.Mark;
import mjc.entity.Payment;
import mjc.entity.Student;
import mjc.mapper.MarkMapper;
import mjc.mapper.PaymentMapper;
import mjc.mapper.StudentMapper;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class JoinTest {

    @Test
    public void selectMonthlyPaymentTest() {
        //given
        String autoCodeQuery = "select pay.* from Payment pay join PaymentType type on type.id = pay.type where type.name = 'MONTHLY'";

        //when
        JdbcTemplate connection = SqlConnection.getConnection();
        List<Payment> expectedResult = connection.query(autoCodeQuery, new PaymentMapper());

        //then
        Assertions.assertEquals(expectedResult.size(), 779);
    }

    @Test
    public void selectMarksByArtTest() {
        //given
        String autoCodeQuery = "select mark.* from Mark mark join Subject subject on mark.subject_id = subject.id where subject.name = 'Art'";

        //when
        JdbcTemplate connection = SqlConnection.getConnection();
        List<Mark> expectedResult = connection.query(autoCodeQuery, new MarkMapper());

        //then
        Assertions.assertEquals(expectedResult.size(), 5);
    }

    @Test
    public void selectStudentWithWeeklyPaymentTest() {
        //given
        String autoCodeQuery = "select student.* from Student student where exists (select 1 from Payment pay " +
                "join PaymentType type on type.Id = pay.type where pay.student_id = student.Id and type.name = 'WEEKLY')";

        //when
        JdbcTemplate connection = SqlConnection.getConnection();
        List<Student> expectedResult = connection.query(autoCodeQuery, new StudentMapper());

        //then
        Assertions.assertEquals(expectedResult.size(), 615);
    }

    @Test
    public void selectStudentWithMarksByMathTest() {
        //given
        String autoCodeQuery = "select student.* from Student student where exists (select 1 from Mark mark " +
                "join Subject subject on mark.subject_id = subject.id where subject.name = 'Math' and mark.student_id = student.Id)";

        //when
        JdbcTemplate connection = SqlConnection.getConnection();
        List<Student> expectedResult = connection.query(autoCodeQuery, new StudentMapper());

        //then
        Assertions.assertEquals(expectedResult.size(), 12);
    }
}
